# Logan-aggregate

System to concatenate results from Logan-analysis. See the `logan-analysis` repo, it's the same instructions.

## Example steps

For the Prodigal run:

    cd sets
    \time bash ../utils/parallel_listing.sh s3://serratus-rayan/beetles/logan_oct7_run/prodigal oct7-prodigal.txt

    #optional
    grep -Fwf sra_taxid_human.txt oct7-prodigal.txt > oct7-prodigal-human.listing.txt
    awk '{print "s3://serratus-rayan/beetles/logan_oct7_run/prodigal/"$4}' oct7-prodigal-human.listing.txt  > oct7-prodigal-human.txt

    #check size of processed data
    awk '{s+=$3} END {print s}' oct7-prodigal.human.txt

    cd ..
    echo oct7-prodigal-human > set
    bash run_test.sh # modify with a few test sets
    bash process_array.sh human 100

For 12 TB results of a diamond run:

    cd sets
    \time bash ../utils/parallel_listing.sh s3://serratus-rayan/beetles/logan_feb6_run/diamond feb6-diamond.txt
    awk '{print "s3://serratus-rayan/beetles/logan_feb6_run/diamond/"$4}' feb6-diamond.txt > feb6-diamond-files.txt
    cd ..

    echo feb6-diamond-files > set
    bash run_test.sh # modify with a few test sets                                                                                                
    bash process_array.sh petadex-orfs-hmm-filtered 2000
    # for some reason, aggregation failed fr the heaviest of the jobs when i was splitting in less than 2000. Maybe something to do with disk storage, though unlikely. Never debugged it.
