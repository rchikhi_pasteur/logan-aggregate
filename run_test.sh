#!/bin/bash
set -e
bucket=$(if [[ -z $(aws sts get-caller-identity |grep serratus-rayan) ]]; then echo "logan-dec2023-testbucket"; else echo "logan-testing-march2024"; fi)
#echo s3://serratus-rayan/beetles/logan_oct7_run/prodigal/SRR7101251/SRR7101251.prodigal.fa.zst    > sets/test-array.txt
#echo s3://serratus-rayan/beetles/logan_oct7_run/prodigal/SRR26195076/SRR26195076.prodigal.fa.zst >> sets/test-array.txt
#echo s3://serratus-rayan/beetles/logan_oct7_run/prodigal/SRR7101251/SRR7101251.prodigal.fa.zst   >> sets/test-array.txt
#echo s3://serratus-rayan/beetles/logan_oct7_run/prodigal/SRR26195076/SRR26195076.prodigal.fa.zst >> sets/test-array.txt
echo s3://serratus-rayan/beetles/logan_feb6_run/diamond/DRR000001/DRR000001.diamond.feb6.txt  > sets/test-array.txt
echo s3://serratus-rayan/beetles/logan_feb6_run/diamond/DRR000002/DRR000002.diamond.feb6.txt >> sets/test-array.txt
echo s3://serratus-rayan/beetles/logan_feb6_run/diamond/DRR000982/DRR000982.diamond.feb6.txt >> sets/test-array.txt
echo s3://serratus-rayan/beetles/logan_feb6_run/diamond/DRR000852/DRR000852.diamond.feb6.txt >> sets/test-array.txt


mv set set.bak && echo "test-array" > set

bash -c "cd batch && bash deploy-docker.sh"

bash process_array.sh test 2

rm -f sets/test-array && mv set.bak set
