#!/bin/bash
# this version has no hardcoded parameters, it's all passed

set -eu

# Initialize variables to hold the last executed command and its line number.
LAST_CMD=""
LAST_CMD_LINE=0

# Function to log the last command executed (for DEBUG trap).
log_last_command() {
    LAST_CMD="$BASH_COMMAND"
    LAST_CMD_LINE="$BASH_LINENO"
}

handle_error() {
    local error_code=$?
    echo "Error occurred in COPY function at line $LAST_CMD_LINE: '$LAST_CMD' exited with status $error_code."
}

cleanup() {
    if [[ -n "$jobid" ]]; then
        rm -Rf /localdisk/"$jobid"
    fi
    echo "Cleanup of $jobid complete."
}

# We define a bash function that:
#  1) Compresses the input .txt with zstd
#  2) Extracts the "full_qseq" field into a FASTA
#  3) Calls ORFs (6 frames, non-ATG starts) using EMBOSS getorf
process_diamond_file() {
    txtfile="$1"
	base=$(basename "$txtfile" .txt)
    
	# 2) Create FASTA of full_qseq, but only for lines with evalue < 1e-10
	cat "$txtfile" | \
		awk '$11 < 1e-10 { print ">" $1 "|" $6 "\n" $NF }' \
		> "${base}.full_qseq.fa"

    mkdir tmp-$base

	orfipy "${base}.full_qseq.fa" \
	  --pep "${base}.orfs.fa" \
      --outdir tmp-$base \
	  --between-stops \
	  --include-stop \
	  --strand b \
	  --min 30 \
	  --single-mode

    mv tmp-$base/${base}.orfs.fa .
    rm -Rf tmp-$base/

	hmmsearch -o $base.hmmsearch.txt --tblout ${base}.hits.tbl --cpu 3 -E 1e-5 /tasks/aggregate_feb6.petadex_hmm1.hmm "${base}.orfs.fa"

    cut -f1 $base.hits.tbl | sort | uniq > $base.keep.ids
    seqtk subseq $base.orfs.fa $base.keep.ids > $base.orfs_with_hmmer_hits.fa

    echo "Finished processing ${txtfile}"
}

export -f process_diamond_file

task() {
	trap 'log_last_command' DEBUG
    trap 'handle_error $LINENO' ERR
    trap 'cleanup' EXIT
	
	locals3file=$1
    THREADS=$2
    jobid=$3
    outdate=$4
    dest=$5

    echo "Logan aggregate ($dest, $outdate) task for jobid $jobid, $THREADS threads, file list: $locals3file"
    
    mkdir -p /localdisk/$jobid/data
   
	cat "$locals3file" | parallel -j "$THREADS" '
	echo "Downloading file {}"
	if ! \time s5cmd cp -c '"$THREADS"' {} '"/localdisk/$jobid/data/"' ; then
		echo "Error with s5cmd cp, cleaning up"
		rm -Rf '"/localdisk/$jobid"'
		exit 1  # This ensures the error trap is triggered if s5cmd cp fails
	fi
    
	# Check if file is empty and delete it
    local_file=/localdisk/'"$jobid"'/data/$(basename {})
    if [ ! -s "$local_file" ]; then
        echo "File $local_file is empty, deleting"
        rm -f "$local_file"
    fi 
	'

    cd /localdisk/"$jobid" || exit

	# Use GNU Parallel to process all .txt files in 'data/' in parallel
    find data -name "*.txt" | parallel -j $THREADS process_diamond_file

	#cat *.orfs.fa |zstd -c                 > "${jobid}-all_raw_orfs.fa.zst"
    find -name "*.hits.tbl"|xargs cat|zstd -c                 > "${jobid}-hmm_hits.tbl.zst"
    find -name "*.orfs_with_hmmer_hits.fa" |xargs cat|zstd -c > "${jobid}-hmm_filtered_orfs.fa.zst"

    # upload results
    # opted against, because too big
	#file="${jobid}-all_raw_orfs.fa.zst"
    #[ -f $file ] && s5cmd cp $file $dest
	file="${jobid}-hmm_filtered_orfs.fa.zst"
    [ -f $file ] && s5cmd cp $file $dest
    file="${jobid}-hmm_hits.tbl.zst"
    [ -f $file ] && s5cmd cp $file $dest


    rm -Rf /localdisk/"$jobid"
	echo "Done with $jobid"
	trap '' EXIT
}

export -f task
