#!/bin/bash
# this version has no hardcoded parameters, it's all passed

set -eu

# Initialize variables to hold the last executed command and its line number.
LAST_CMD=""
LAST_CMD_LINE=0

# Function to log the last command executed (for DEBUG trap).
log_last_command() {
    LAST_CMD="$BASH_COMMAND"
    LAST_CMD_LINE="$BASH_LINENO"
}

handle_error() {
    local error_code=$?
    echo "Error occurred in COPY function at line $LAST_CMD_LINE: '$LAST_CMD' exited with status $error_code."
}

cleanup() {
    if [[ -n "$jobid" ]]; then
        rm -Rf /localdisk/"$jobid"
    fi
    echo "Cleanup of $jobid complete."
}

task() {
	trap 'log_last_command' DEBUG
    trap 'handle_error $LINENO' ERR
    trap 'cleanup' EXIT
	
	locals3file=$1
    THREADS=$2
    jobid=$3
    outdate=$4
    dest=$5

    echo "Logan aggregate ($dest, $outdate) task for jobid $jobid, $THREADS threads, file list: $locals3file"
    
    mkdir -p /localdisk/$jobid/data
   
	cat "$locals3file" | parallel -j "$THREADS" '
	echo "Downloading file {}"
	if ! \time s5cmd cp -c '"$THREADS"' {} '"/localdisk/$jobid/data/"' ; then
		echo "Error with s5cmd cp, cleaning up"
		rm -Rf '"/localdisk/$jobid"'
		exit 1  # This ensures the error trap is triggered if s5cmd cp fails
	fi
	'

    cd /localdisk/"$jobid" || exit

	# Create named pipes (FIFOs)
	mkfifo complete.fifo partial.fifo

	# Start zstd in the background, reading from the FIFOs and writing compressed output
	zstd -c < complete.fifo > $jobid-complete.fa.zst &
	zstd -c < partial.fifo > $jobid-partial.fa.zst &

	# Process the input and route the data to the appropriate FIFO
    find data -name '*.zst' -print0 | xargs -0 zstdcat | \
		awk '
		/^>/ {
		output = ($0 ~ /partial=00/) ? "complete.fifo" : "partial.fifo";
	}
	{ print > output }
	'

	# Clean up by removing the FIFOs after processing is done
	rm complete.fifo partial.fifo

    [ -f $jobid-complete.fa.zst ] && aws s3 cp $jobid-complete.fa.zst $dest
    [ -f $jobid-partial.fa.zst  ] && aws s3 cp $jobid-partial.fa.zst $dest

    rm -Rf /localdisk/"$jobid"
	echo "Done with $jobid"
	trap '' EXIT
}

export -f task
