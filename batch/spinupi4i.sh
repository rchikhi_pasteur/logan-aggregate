if [ "$1" == "--update" ]; then
    echo "updating stack.."
    aws cloudformation update-stack --stack-name Logan-Aggregate --template-body file://templatei4i.yaml --capabilities CAPABILITY_NAMED_IAM
else
    aws cloudformation create-stack --stack-name Logan-Aggregate --template-body file://templatei4i.yaml --capabilities CAPABILITY_NAMED_IAM
fi
