#!/bin/bash
set -euo pipefail 
aws sts get-session-token --duration-seconds 6000 > credentials.json

echo "AWS_ACCESS_KEY_ID=$(jq -r '.Credentials.AccessKeyId' credentials.json)" > credentials.env
echo "AWS_SECRET_ACCESS_KEY=$(jq -r '.Credentials.SecretAccessKey' credentials.json)" >> credentials.env 
echo "AWS_SESSION_TOKEN=$(jq -r '.Credentials.SessionToken' credentials.json)" >> credentials.env 


bucket=$(if [[ -z $(aws sts get-caller-identity |grep serratus-rayan) ]]; then echo "logan-dec2023-testbucket"; else echo "logan-testing-march2024"; fi)

#echo s3://serratus-rayan/beetles/logan_oct7_run/prodigal/SRR7101251/SRR7101251.prodigal.fa.zst    > array_1c.txt
#echo s3://serratus-rayan/beetles/logan_oct7_run/prodigal/SRR26195076/SRR26195076.prodigal.fa.zst >> array_1c.txt
#echo s3://serratus-rayan/beetles/logan_oct7_run/pyrodigal/DRR000003/DRR000003.pyrodigal.fa.zst   >> array_1c.txt
echo s3://serratus-rayan/beetles/logan_feb6_run/diamond/DRR000001/DRR000001.diamond.feb6.txt  > array_1c.txt
echo s3://serratus-rayan/beetles/logan_feb6_run/diamond/DRR000002/DRR000002.diamond.feb6.txt >> array_1c.txt
echo s3://serratus-rayan/beetles/logan_feb6_run/diamond/DRR000982/DRR000982.diamond.feb6.txt >> array_1c.txt
echo s3://serratus-rayan/beetles/logan_feb6_run/diamond/DRR000852/DRR000852.diamond.feb6.txt >> array_1c.txt
echo s3://serratus-rayan/beetles/logan_feb6_run/diamond/DRR110881/DRR110881.diamond.feb6.txt >> array_1c.txt


s3file=s3://$bucket/array_1c_agg.txt
aws s3 cp array_1c.txt $s3file

docker build -t logan-aggregate-job-x86_64 . 
docker run \
    --env-file credentials.env \
    logan-aggregate-job-x86_64 \
    -i $s3file -p test -t 4

rm -f credentials.env credentials.json
