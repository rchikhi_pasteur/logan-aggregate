#!/bin/bash
# ENTRYPOINT SCRIPT ===================
# logan-aggregate.sh
# =====================================
set -eu

# modify me
outdate=feb6
task=aggregate_${outdate}
dest=s3://serratus-rayan/beetles/logan_${outdate}_run/diamond-concat/

# those parameters are passed by the batch system 
function usage {
  echo ""
  echo "Usage: docker run logan-aggregate-job-x86_64 [OPTIONS]"
  echo ""
  echo "    -h    Show this help/usage message"
  echo ""
  echo "    Required Fields"
  echo "    -i    S3 path of list of Logan unitigs/contigs to process [s3://bucket/file.txt]"
  echo "    -p    Output prefix [test]"
  echo "    -t    Number of threads"
  echo ""
  echo 'ex: docker build -t logan-aggregate-job-x86_64 . && docker run logan-aggregate-job-x86_64 -i s3://bucket/file.txt -o testbucket'
  false
  exit 1
}

S3FILE=''
PREFIX='test'
VERBOSE='false'

function log () {
    if [[ $VERBOSE == 'TRUE' ]]
    then
        echo "$@"
    fi
}

while getopts i:p:t:vh FLAG; do
  case $FLAG in
    # Search Files  -----------
    i)
      S3FILE=$OPTARG
      ;;
    p)
      PREFIX=$OPTARG
      ;;
    t)
      THREADS=$OPTARG
      ;;
	v)
      VERBOSE='TRUE'
      ;;
    h)  #show help ----------
      usage
      ;;
    \?) #unrecognized option - show help
      echo "Input parameter not recognized"
      usage
      ;;
  esac
done

if [ -z "$S3FILE" ]; then
  echo "Error: Input file (-i) is not set."
  usage
fi

if [ -z "$PREFIX" ]; then
  echo "Error: Prefix name (-p) is not set."
  usage
fi

source tasks/$task.sh

echo "Logan aggregate, task: $task"
echo "Number of threads: $THREADS"
# get instance type
TOKEN=`curl -X PUT "http://169.254.169.254/latest/api/token" -s -H "X-aws-ec2-metadata-token-ttl-seconds: 21600"`
instance_type=$(curl -H "X-aws-ec2-metadata-token: $TOKEN" -s http://169.254.169.254/latest/meta-data/instance-type)
echo "Instance type: $instance_type"

date
df -h / /localdisk

jobid="nojobid"

# Check if Array Job
if [[ -z "${AWS_BATCH_JOB_ARRAY_INDEX-}" ]]
then
    echo "Not an array job"
    # get a random number
    jobid=${PREFIX}-$(date +%s%N | cut -b10-19)
else
    echo "Array job: ${AWS_BATCH_JOB_ARRAY_INDEX-}"
    printf -v padded_number "%05d" ${AWS_BATCH_JOB_ARRAY_INDEX-}
    S3FILE="$S3FILE"$padded_number
    jobid="${PREFIX}-$padded_number"
fi

# grab the list of accessions
locals3file=s3file_$jobid.txt
s5cmd cp -c 1 $S3FILE $locals3file
nb_files=$(wc -l < $locals3file)
echo "$nb_files files to process"

task $locals3file $THREADS $jobid $outdate $dest

echo "Logan aggregate, all done!"
date
df -h / /localdisk
